@extends('layout.master')
@section('judul')
Halaman Tampil User
@endsection
@section('content')

<a href="/user/create" class="btn btn-sm btn-primary rounded-0">Tambah User</a>

<table class="table">
    <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Username</th>
            <th scope="col">Email</th>
            <th scope="col">Act</th>

        </tr>
    </thead>
    <tbody>
        @forelse ($user as $key => $item)
        <tr>
            <th scope="row">{{$key+1}}</th>
            <td>{{$item->username}}</td>
            <td>{{$item->email}}</td>

            <td>
                <form action="/user/{{$item->id}}" method="post">
                    @csrf
                    @method('delete')
                    <a href="/user/{{$item->id}}" class="btn btn-info btn-sm rounded-0">Detail Data</a>
                    <a href="/user/{{$item->id}}/edit" class="btn btn-primary btn-sm rounded-0">Edit Data</a>
                    <input type="submit" class="btn btn-danger btn-sm rounded-0" value="Delete">
                </form>
            </td>


        </tr>

        @empty
        <tr>
            <td>tidak ada user</td>
        </tr>

        @endforelse
    </tbody>
</table>

@endsection