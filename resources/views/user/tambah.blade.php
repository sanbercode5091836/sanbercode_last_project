@extends('layout.master')
@section('judul')
Halaman Tambah User
@endsection
@section('content')

<form action="/user" method="POST">
    @csrf
    <div class="form-group">
        <label>Username</label>
        <input type="text" class="form-control @error('username') is-invalid @enderror" name="username" placeholder="Username">
        <!-- <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small> -->
    </div>
    @error('username')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <div class="form-group">
        <label>Email</label>
        <input type="email" class="form-control @error('email') is-invalid @enderror" name="email" placeholder="Email">
    </div>
    @error('email')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <div class="form-group">
        <label>Password</label>
        <input type="password" class="form-control @error('password') is-invalid @enderror" name="password" placeholder="Password">
    </div>
    @error('password')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <button type="submit" class="btn btn-primary rounded-0">Submit</button>
    <a href="/user/tampil" class="btn btn-primary rounded-0">Kembali</a>
</form>

@endsection