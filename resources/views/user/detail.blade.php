@extends('layout.master')
@section('judul')
Halaman Detail User
@endsection
@section('content')

<div class="card" style="width: 15rem;">
    <img class="card-img-top" src="{{asset('/template/dist/img/ava.png')}}" alt="Card image cap">
    <div class="card-body">
        <hr>
        <label for="">Username : </label>
        <p class="badge badge-pill badge-success">{{$userData->username}}</p>
        <p>
            <label for="">Email : </label>
            <a href="#">{{$userData->email}}</a>
        </p>
    </div>
</div>
<hr>
<a href="/user/tampil" class="btn btn-primary rounded-0">Kembali</a>


@endsection