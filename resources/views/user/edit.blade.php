@extends('layout.master')
@section('judul')
Halaman Edit User
@endsection
@section('content')

<form action="/user/{{$userData->id}}" method="post">
    @csrf
    @method('put')
    <div class="form-group">
        <label>Username</label>
        <input type="text" value="{{$userData->username}}" class="form-control @error('username') is-invalid @enderror" name="username" readonly>
        <!-- <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small> -->
    </div>
    @error('username')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <div class="form-group">
        <label>Email</label>
        <input type="text" value="{{$userData->email}}" class="form-control @error('email') is-invalid @enderror" name="email">
    </div>
    @error('email')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <div class="form-group">
        <label>Password</label>
        <input type="password" value="{{$userData->password}}" class="form-control @error('password') is-invalid @enderror" name="password">
    </div>
    @error('password')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <button type="submit" class="btn btn-primary rounded-0">Update</button>
    <a href="/user/tampil" class="btn btn-primary rounded-0">Batal</a>
</form>

@endsection