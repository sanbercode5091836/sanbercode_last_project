<div class="sidebar">
  <!-- Sidebar user (optional) -->
  <div class="user-panel mt-3 pb-3 mb-3 d-flex">
    <div class="image">
      <img src="{{asset('/template/dist/img/user2-160x160.jpg')}}" class="img-circle elevation-2" alt="User Image">
    </div>
    <div class="info">
      <a href="/" class="d-block">Alexander Pierce</a>
    </div>
  </div>

  <!-- SidebarSearch Form -->
  <div class="form-inline">
    <div class="input-group" data-widget="sidebar-search">
      <input class="form-control form-control-sidebar" type="search" placeholder="Search" aria-label="Search">
      <div class="input-group-append">
        <button class="btn btn-sidebar">
          <i class="fas fa-search fa-fw"></i>
        </button>
      </div>
    </div>
  </div>

  <!-- Sidebar Menu -->
  <nav class="mt-2">
    <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
      <!-- Add icons to the links using the .nav-icon class
             with font-awesome or any other icon font library -->



      @auth

      <li class="nav-item">
        <a href="/erd" class="nav-link">
          <i class="nav-icon fas fa-sitemap"></i>
          <p>
            ERD
            <i class="right fas"></i>
          </p>
        </a>
      </li>

      <li class="nav-item">
        <a href="/user" class="nav-link">
          <i class="nav-icon fas fa-id-card"></i>
          <p>
            Data User
            <i class="right fas"></i>
          </p>
        </a>
      </li>

      <li class="nav-item">
        <a href="/buku" class="nav-link">
          <i class="nav-icon fas fa-th-list"></i>
          <p>
            Buku
          </p>
        </a>
      </li>

      <li class="nav-item">
        <a href="/peminjaman" class="nav-link">
          <i class="nav-icon fas fa-book"></i>
          <p>
            Peminjaman
          </p>
        </a>
      </li>

      <li class="nav-item">
        <a href="/stok" class="nav-link">
          <i class="nav-icon fas fa-table"></i>
          <p>
            Data Buku
          </p>
        </a>
      </li>

      <li class="nav-item">
        <a href="{{ route('logout') }}" onclick="event.preventDefault();
         document.getElementById('logout-form').submit();">
          {{ __('Logout') }}
          <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
            @csrf
          </form>
          <i class="nav-icon fa fa-users"></i>
          <p>

          </p>
        </a>
      </li>
      @endauth

      @guest
      <li class="nav-item">
        <a href="/login" class="nav-link">
          <i class="nav-icon fa fa-user"></i>
          <p>
            Login
          </p>
        </a>
      </li>
      @endguest

  </nav>
  <!-- /.sidebar-menu -->
</div>