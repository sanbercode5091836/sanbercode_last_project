@extends('layout.master')
@section('judul')
Halaman Detail Buku
@endsection
@section('content')

<div class="card" style="width: 15rem;">
    <img class="card-img-top" src="{{asset('/template/dist/img/default-150x150.png')}}" alt="Card image cap">
    <div class="card-body">
        <a href="" class="btn btn-primary rounded-0">{{$bukuData->judul}}</a>
        <p>
            <label for="">Pengarang : </label>
            <a href="#">{{$bukuData->pengarang}}</a>
        </p>
        <p>
            <label for="">Penerbit : </label>
            <a href="#">{{$bukuData->penerbit}}</a>
        </p>
        <label for="">Tahun : </label>
        <p class="badge badge-pill badge-success">{{$bukuData->tahun}}</p>
    </div>
</div>
<hr>
<a href="/buku/tampil" class="btn btn-primary rounded-0">Kembali</a>


@endsection