@extends('layout.master')
@section('judul')
Halaman Edit Buku
@endsection
@section('content')

<form action="/buku/{{$bukuData->id}}" method="post">
    @csrf
    @method('put')
    <div class="form-group">
        <label>Judul Buku</label>
        <input type="text" value="{{$bukuData->judul}}" class="form-control @error('judul') is-invalid @enderror" name="judul" placeholder="Judul Buku">
        <!-- <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small> -->
    </div>
    @error('judul')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <div class="form-group">
        <label>Pengarang</label>
        <input type="text" value="{{$bukuData->pengarang}}" class="form-control @error('pengarang') is-invalid @enderror" name="pengarang" placeholder="Pengarang">
    </div>
    @error('pengarang')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <div class="form-group">
        <label>Penerbit</label>
        <input type="text" value="{{$bukuData->penerbit}}" class="form-control @error('penerbit') is-invalid @enderror" name="penerbit" placeholder="Penerbit">
    </div>
    @error('penerbit')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <div class="form-group">
        <label>Tahun</label>
        <input type="number" value="{{$bukuData->tahun}}" class="form-control @error('tahun') is-invalid @enderror" name="tahun" placeholder="Tahun">
    </div>
    @error('tahun')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror


    <button type="submit" class="btn btn-primary rounded-0">Update</button>
    <a href="/buku/tampil" class="btn btn-primary rounded-0">Batal</a>
</form>

@endsection