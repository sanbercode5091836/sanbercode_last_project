@extends('layout.master')
@section('judul')
Halaman Tampil Buku
@endsection
@section('content')

<a href="/buku/create" class="btn btn-sm btn-primary rounded-0">Tambah Buku</a>

<table class="table">
    <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Judul</th>
            <th scope="col">Pengarang</th>
            <th scope="col">Penerbit</th>
            <th scope="col">Tahun</th>
            <th scope="col">Act</th>

        </tr>
    </thead>
    <tbody>
        @forelse ($buku as $key => $item)
        <tr>
            <th scope="row">{{$key+1}}</th>
            <td>{{$item->judul}}</td>
            <td>{{$item->pengarang}}</td>
            <td>{{$item->penerbit}}</td>
            <td>{{$item->tahun}}</td>

            <td>
                <form action="/buku/{{$item->id}}" method="post">
                    @csrf
                    @method('delete')
                    <a href="/buku/{{$item->id}}" class="btn btn-info btn-sm rounded-0">Detail Data</a>
                    <a href="/buku/{{$item->id}}/edit" class="btn btn-primary btn-sm rounded-0">Edit Data</a>
                    <input type="submit" class="btn btn-danger btn-sm rounded-0" value="Delete">
                </form>
            </td>


        </tr>

        @empty
        <tr>
            <td>tidak ada cast</td>
        </tr>

        @endforelse
    </tbody>
</table>

@endsection