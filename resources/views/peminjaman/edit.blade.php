@extends('layout.master')
@section('judul')
   Edit Pinjaman
@endsection
@section('content')
    
<form action="/pinjam/{{$peminjaman->id}}" method="POST">
    @csrf
    @method('PUT')
    <div class="form-group">
      <label for="exampleFormControlSelect1">Nama</label>
      <select class="form-control" id="exampleFormControlSelect1" name="user_id">
        <option value="{{ $peminjaman->user_id }}"></option>
        @foreach ($user as $user)
        <option value="{{ $user->id }}">{{ $user->username }}</option>
        @endforeach
      </select>
      @error('user_id')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
      @enderror
    </div>
    <div class="form-group">
      <label for="exampleFormControlSelect1">Buku</label>
      <select class="form-control" id="exampleFormControlSelect1" name="buku_id">
        <option value="{{ $peminjaman->buku_id }}"></option>
        @foreach ($buku as $buku)
        <option value="{{ $buku->id }}">{{ $buku->judul }}</option>
        @endforeach
      </select>
      @error('buku_id')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
      @enderror
    </div>
    <div class="form-group">
      <label for="Tanggal Pinjam">Tanggal Pinjam</label>
      <input type="date" name="tanggal_pinjam" id="" value="{{ $peminjaman->tanggal_pinjam }}">
    </div>
    <div class="form-group">
      <label for="Tanggal kembali">Tanggal Kembali</label>
      <input type="date" name="tanggal_kembali" id="" value="{{ $peminjaman->tanggal_kembali }}">
    </div>
    <button type="submit" class="btn btn-primary">Tambah</button>
  </form>
@endsection