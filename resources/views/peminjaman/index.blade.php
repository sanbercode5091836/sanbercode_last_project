@extends('layout.master')
@section('judul')
   pinjam
@endsection

@section('content')
<a href="/pinjam/create" class="btn btn-primary mb-2">Tambah Pinjam</a>
@push('script')
<script src="{{ asset('/template/plugins/datatables/jquery.dataTables.js') }}"></script>
<script src="{{ asset('/template/plugins/datatables-bs4/js/dataTables.bootstrap4.js') }}"></script>
<script>
  $(function () {
    $("#peminjaman").DataTable();
  });
</script>
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.11.3/datatables.min.css"/>
@endpush
<table class="table" id="peminjaman">
    <thead class="thead-light">
      <tr>
        <th scope="col">#</th>
        <th scope="col">Nama</th>
        <th scope="col">Buku</th>
        <th scope="col">Tanggal Pinjam</th>
        <th scope="col">Tanggal Kembali</th>
        <th scope="col">Actions</th>
      </tr>
    </thead>
    <tbody>
        @forelse ($peminjaman as $key=>$value)
            <tr>
                <td>{{$key + 1}}</th>
                <td>{{$value->username}}</td>
                <td>{{$value->judul}}</td>
                <td>{{$value->tanggal_pinjam}}</td>
                <td>{{$value->tanggal_kembali}}</td>
                <td>
                    <a href="/pinjam/{{$value->id}}/edit" class="btn btn-primary d-inline">Edit</a>
                    <form action="/pinjam/{{$value->id}}" method="POST" class="d-inline">
                        @csrf
                        @method('DELETE')
                        <input type="submit" class="btn btn-danger my-1" value="Delete">
                    </form>
                    
                </td>
            </tr>
        @empty
            <tr colspan="3">
                <td>No data</td>
            </tr>  
        @endforelse              
    </tbody>
</table>
@endsection