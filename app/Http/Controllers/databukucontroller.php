<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class databukucontroller extends Controller
{
    public function create()
    {
        return view('buku.tambah');
    }

    public function store(Request $request)
    {
        // ini untuk validasi inputan form
        $request->validate([
            'judul' => 'required|min:4',
            'pengarang' => 'required',
            'penerbit' => 'required',
            'tahun' => 'required',

        ]);

        // ini untuk insert
        DB::table('buku')->insert([
            'judul' => $request->judul,
            'pengarang' => $request->pengarang,
            'penerbit' => $request->penerbit,
            'tahun' => $request->tahun,
        ]);

        // redirect ke hal cast

        return redirect('/buku');
    }

    public function index()
    {
        $buku = DB::table('buku')->get();
        return view('buku.tampil', ['buku' => $buku]);
        return view('buku.tampil', compact('buku')); //compact digunakan untuk mengirim data ke view, perhatiakn bagian -cast.tampil- (cast nama folder, tampil itu filenya buat halnya)
    }

    public function show($id)
    {
        $bukuData = DB::table('buku')->find($id);
        return view('buku.detail', ['bukuData' => $bukuData]);
    }

    public function edit($id)
    {
        $bukuData = DB::table('buku')->find($id);
        return view('buku.edit', ['bukuData' => $bukuData]);
    }

    public function update($id, Request $request)
    {
        // ini untuk validasi inputan form
        $request->validate([
            'judul' => 'required|min:4',
            'pengarang' => 'required',
            'penerbit' => 'required',
            'tahun' => 'required',
        ]);

        //upatate data
        DB::table('buku')
            ->where('id', $id)
            ->update(
                [
                    'judul' => $request->input('judul'),
                    'pengarang' => $request->input('pengarang'),
                    'penerbit' => $request->input('penerbit'),
                    'tahun' => $request->input('tahun')
                ]
            );
        return redirect('/buku');
    }

    //delete data

    public function destroy($id)
    {
        DB::table('buku')->where('id', '=', $id)->delete();
        return redirect('/buku/tampil');
    }
}
