<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\PeminjamanModel;


class PeminjamanBukuController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $peminjaman = DB::table('peminjaman')
        ->join('user', 'user.id', '=', 'peminjaman.user_id')
        ->join('buku', 'buku.id', '=', 'peminjaman.buku_id')
        ->get();
        // dd($peminjaman);
        // dd($peminjaman);
        return view('peminjaman.index', compact('peminjaman'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $user = DB::table('user')->get();
        $buku = DB::table('buku')->get();
        $res["user"]=$user;
        $res["buku"]=$buku;
        return view('peminjaman.create', $res);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        // dd($request->tanggal_pinjam);
        $request->validate([
            'tanggal_pinjam' => 'required',
            'tanggal_kembali' => 'required',
            'user_id' => 'required',
            'buku_id' => 'required',
        ]);
        $query = DB::table('peminjaman')->insert([
            "tanggal_pinjam" => $request["tanggal_pinjam"],
            "tanggal_kembali" => $request["tanggal_kembali"],
            "user_id" => $request["user_id"],
            "buku_id" => $request["buku_id"]
        ]);
        return redirect('/peminjaman');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $peminjaman = DB::table('peminjaman')->where('id', $id)->first();
        $user = DB::table('user')->get();
        $buku = DB::table('buku')->get();
        $res["user"]=$user;
        $res["buku"]=$buku;
        $res["peminjaman"]=$peminjaman;
        return view('peminjaman.edit',$res);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $request->validate([
            'tanggal_pinjam' => 'required',
            'tanggal_kembali' => 'required',
            'user_id' => 'required',
            'buku_id' => 'required',
        ]);
        $query = DB::table('peminjaman')->where('id', $id)
        ->update([
            "tanggal_pinjam" => $request["tanggal_pinjam"],
            "tanggal_kembali" => $request["tanggal_kembali"],
            "user_id" => $request["user_id"],
            "buku_id" => $request["buku_id"]
        ]);
        return redirect('/peminjaman');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $query = DB::table('peminjaman')->where('id', $id)->delete();
        return redirect('/peminjaman');
    }
}
