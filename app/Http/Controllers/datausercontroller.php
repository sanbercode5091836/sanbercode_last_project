<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class datausercontroller extends Controller
{
    public function create()
    {
        return view('user.tambah');
    }

    public function store(Request $request)
    {
        // ini untuk validasi inputan form
        $request->validate([
            'username' => 'required|min:4',
            'email' => 'required',
            'password' => 'required',

        ]);

        // ini untuk insert
        DB::table('user')->insert([
            'username' => $request->username,
            'email' => $request->email,
            'password' => $request->password,
        ]);

        // redirect ke hal cast

        return redirect('/user');
    }

    public function index()
    {
        $user = DB::table('user')->get();
        return view('user.tampil', ['user' => $user]);
        return view('user.tampil', compact('user')); //compact digunakan untuk mengirim data ke view, perhatiakn bagian -cast.tampil- (cast nama folder, tampil itu filenya buat halnya)
    }


    public function show($id)
    {
        $userData = DB::table('user')->find($id);
        return view('user.detail', ['userData' => $userData]);
    }

    public function edit($id)
    {
        $userData = DB::table('user')->find($id);
        return view('user.edit', ['userData' => $userData]);
    }

    public function update($id, Request $request)
    {
        // ini untuk validasi inputan form
        $request->validate([
            'username' => 'required|min:4',
            'email' => 'required',
            'password' => 'required',
        ]);

        //upatate data
        DB::table('user')
            ->where('id', $id)
            ->update(
                [
                    'username' => $request->input('username'),
                    'email' => $request->input('email'),
                    'password' => $request->input('password')
                ]
            );
        return redirect('/user');
    }

    //delete data

    public function destroy($id)
    {
        DB::table('user')->where('id', '=', $id)->delete();
        return redirect('/user/tampil');
    }
}
