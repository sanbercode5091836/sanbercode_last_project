<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\homecontroller;
use App\Http\Controllers\PeminjamanBukuController;
use App\Http\Controllers\datausercontroller;
use App\Http\Controllers\databukucontroller;
use App\Http\Controllers\erdcontroller;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [homecontroller::class, 'home']);
Route::get('/stok', [homecontroller::class, 'stok']);

Route::get('/peminjaman', [PeminjamanBukuController::class, 'index']);
Route::get('/pinjam/create', [PeminjamanBukuController::class, 'create']);
Route::post('/pinjam', [PeminjamanBukuController::class, 'store']);
Route::get('/pinjam/{pinjam_id}/edit', [PeminjamanBukuController::class, 'edit']);
Route::put('/pinjam/{pinjam_id}', [PeminjamanBukuController::class, 'update']);
Route::delete('/pinjam/{pinjam_id}', [PeminjamanBukuController::class, 'destroy']);
// Route::group(['middleware' => 'auth'], function () {

// })

// crud buku
Route::get('/buku', [databukucontroller::class, 'create']);
Route::get('/buku/create', [databukucontroller::class, 'create']);
Route::post('/buku', [databukucontroller::class, 'store']);
Route::get('/buku/tampil', [databukucontroller::class, 'index']);
Route::get('/buku/{id}', [databukucontroller::class, 'show']);
Route::get('/buku/{id}/edit', [databukucontroller::class, 'edit']);
Route::put('/buku/{id}', [databukucontroller::class, 'update']);
Route::delete('/buku/{id}', [databukucontroller::class, 'destroy']);
// end buku

// crud user
Route::get('/user', [datausercontroller::class, 'create']);
Route::get('/user/create', [datausercontroller::class, 'create']);
Route::post('/user', [datausercontroller::class, 'store']);
Route::get('/user/tampil', [datausercontroller::class, 'index']);
Route::get('/user/{id}', [datausercontroller::class, 'show']);
Route::get('/user/{id}/edit', [datausercontroller::class, 'edit']);
Route::put('/user/{id}', [datausercontroller::class, 'update']);
Route::delete('/user/{id}', [datausercontroller::class, 'destroy']);
// end user

Route::get('/erd', [erdcontroller::class, 'desain']);

Auth::routes();
